# Indigo - A simple Grafana dashboard for Windows Server monitoring.

![screenshot](https://gitlab.com/andrew_vanderbilt/indigo/-/raw/main/Screenshot_2023-05-22_at_12.10.58.png)

## Getting started

To install a dashboard into Grafana from a JSON file, follow the steps below:

**Login to your Grafana instance:**

Open your web browser and navigate to your Grafana instance. The default URL would usually be http://localhost:3000 unless you've set it differently. Use your credentials to log in.

**Open The Dashboard Import Interface:**

On the left side of the page, click on the + icon.
Click on the Import option in the expanded menu.

**Upload the JSON file or Paste JSON content:**

There are two ways you can import the dashboard:

Upload a JSON file: Click on the 'Upload .json file' button and navigate to the location of the JSON file on your computer. Select the file and click on 'Open'.


Paste the JSON itself: Alternatively, you can paste the JSON content directly. Open your .json file in a text editor, copy all content and paste it into the 'Or paste JSON' text box in Grafana.

**Customize the Dashboard Settings:**

Grafana may prompt you to select the data source you want to use for the dashboard. Choose the correct one from the dropdown.
You can also change the name of the dashboard, the folder it's saved in, etc.

**Click 'Import':**

When you're ready, click on the 'Import' button to finalize the process.
